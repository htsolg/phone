package ua.rv.huts.oleh.phone;

public interface PhoneConnection {
    void call();
    void sendAMessage();
}
