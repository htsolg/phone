package ua.rv.huts.oleh.phone;

public class NokiaPhone extends Phone implements PhoneConnection {
    public NokiaPhone(String name, String model, double storageCapacity, double ramVolume) {
        super(name, model, storageCapacity, ramVolume);
    }

    @Override
    public void call() {
        System.out.println("Nokia call!!!");
    }

    @Override
    public void sendAMessage() {
        System.out.println("Message from Nokia.");
    }


    @Override
    public String toString() {
        return "NokiaPhone{" +
                "name='" + super.getName() +
                ", model='" + super.getModel() + '\'' +
                ", storageCapacity=" + super.getStorageCapacity() +
                ", ramVolume=" + super.getRamVolume() +
                "} ";
    }
}
