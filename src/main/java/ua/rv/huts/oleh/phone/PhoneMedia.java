package ua.rv.huts.oleh.phone;

public interface PhoneMedia {
    void takePhotos();
    void shootVideo();
}
